package main

import (
	"github.com/visualfc/go-ui/ui"
)

type configUI struct {
	ui.Widget

	buttonClose *ui.Button
}

func NewConfigUI() *configUI {
	return new(configUI).Init()
}

func (p *configUI) Init() *configUI {
	if p.Widget.Init() == nil {
		return nil
	}

	p.buttonClose = ui.NewButtonWithText("Close")
	p.buttonClose.OnClicked(func() {
		p.Close()
	})

	layoutMain := ui.NewVBoxLayout()
	layoutMain.AddWidget(p.buttonClose)

	p.SetWindowTitle("Config")
	p.SetSizev(500, 600)
	p.SetLayout(layoutMain)

	return p
}
