package main

import (
	"github.com/visualfc/go-ui/ui"
)

type mainUI struct {
	ui.Widget

	buttonDownload *ui.Button
	buttonConfig  *ui.Button
	buttonExit    *ui.Button

	configUI *configUI
}

func NewMainUI() *mainUI {
	return new(mainUI).Init()
}

func (p *mainUI) Init() *mainUI {
	if p.Widget.Init() == nil {
		return nil
	}

	p.buttonDownload = ui.NewButtonWithText("Download")
	p.buttonDownload.OnClicked(func() {
	})

	p.buttonConfig = ui.NewButtonWithText("Config")
	p.buttonConfig.OnClicked(func() {
		p.configUI = NewConfigUI()
		p.configUI.Show()
	})

	p.buttonExit = ui.NewButtonWithText("Exit")
	p.buttonExit.OnClicked(func() {
		p.Close()
	})

	layoutMain := ui.NewVBoxLayout()
	layoutMain.AddWidget(p.buttonDownload)
	layoutMain.AddWidget(p.buttonConfig)
	layoutMain.AddWidget(p.buttonExit)

	p.SetWindowTitle("iDownload")
	p.SetSizev(500, 600)
	p.SetLayout(layoutMain)

	return p
}
