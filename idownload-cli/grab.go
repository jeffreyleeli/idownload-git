package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func grabFor(s string) (bs []byte) {
	var e error

	var response *http.Response
	if response, e = http.Get(s); e != nil {
		fmt.Println(e)
		return nil
	}

	defer response.Body.Close()

	if bs, e = ioutil.ReadAll(response.Body); e != nil {
		fmt.Println(e)
		return nil
	}

	return
}
