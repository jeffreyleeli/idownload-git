package main

import (
	"flag"
	"fmt"
	"os"
	"time"
)

func main() {
	t0 := time.Now()

	flag.Parse()

	if *help {
		flag.Usage()
		os.Exit(1)
	}

	urls := []string{
		`http://www.chinajiaoan.cn/you5/onews22.asp?id=`,
	}

	if flag.NArg() > 0 {
		urls = flag.Args()
	}

	for _, v := range urls {
		doFor(v)
	}

	t1 := time.Now()
	fmt.Println("Time:", t1.Sub(t0).Seconds(), "seconds")
}
