package main

import (
	"fmt"
	"os/exec"
)

func download(i int, s string, quit chan bool) {
	id := i + *start
	url := fmt.Sprint(s, id)
	bs := grabFor(url)
	picURL, picName := parseFor(bs)
	downloadFor(picURL, picName)

	fmt.Println(i, id, picName, picURL)

	quit <- true
}

func downloadFor(url, name string) {
	c := exec.Command(*downloader, *downloaderOptions /*"-O "+name, */, url)
	if stdoutput, e := c.Output(); e != nil {
		fmt.Println(e)
	} else {
		fmt.Println(string(stdoutput))
	}
}
