package main

import (
	"strings"
)

func parseFor(bs []byte) (picURL, picName string) {
	if len(bs) == 0 {
		return "", ""
	}

	xml := string(bs)

	head := strings.Index(xml, *target)
	if head < 0 {
		return "", ""
	}
	secHalf := xml[head:]

	tail := strings.Index(secHalf, "\"")
	if tail < 0 {
		return "", ""
	}

	picURL = secHalf[:tail]

	openTag := `<title>`
	head = strings.Index(xml, openTag)
	if head < 0 {
		return "", ""
	}
	secHalf = xml[head+len(openTag):]

	tail = strings.Index(secHalf, `</title>`)
	if tail < 0 {
		return "", ""
	}

	picName = secHalf[:tail]

	return
}
