package main

import (
	"flag"
)

var (
	start             = flag.Int("start", 3192, "Start ID")
	num               = flag.Int("num", 3823-3192, "Number of pictures to download")
	interval          = flag.Int("interval", 1, "ID interval")
	target            = flag.String("target", `http://www.chinajiaoan.cn/uppic/`, "Target string")
	downloader        = flag.String("downloader", "wget", "External downloader")
	downloaderOptions = flag.String("downloaderOptions", "-q", "Options for external downloader")
	help              = flag.Bool("help", false, "Usage help")
)
